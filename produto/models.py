from django.db import models


class Produto(models.Model):
    nome = models.CharField(max_length=150, null=False)
    descricao = models.TextField(null=True)
    unidade = models.CharField(max_length=2, null=True)
    preco_venda = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    data_cadastro = models.DateTimeField(null=True)
    quantidade_emb = models.IntegerField(null=True)
    codigo_ncm = models.CharField(max_length=8, null=True)
    cod_barras_fab = models.CharField(max_length=20, null=True)
    ativo = models.BooleanField(default=True, null=False)

    def __str__(self):
        return self.nome
