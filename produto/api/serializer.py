from rest_framework.serializers import ModelSerializer
from produto.models import Produto


class ProdutoSerializer(ModelSerializer):
    class Meta:
        model = Produto
        fields = ('id',  'nome', 'descricao', 'unidade', 'preco_venda', 'data_cadastro',
                  'quantidade_emb', 'codigo_ncm', 'cod_barras_fab',
                  'ativo')
