from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from produto.models import Produto
from .serializer import ProdutoSerializer

class ProdutoViewSet(ModelViewSet):
    serializer_class = ProdutoSerializer

    def get_queryset(self):
        return Produto.objects.all()

    def list(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(ProdutoViewSet, self).partial_update(request, *args, **kwargs)


