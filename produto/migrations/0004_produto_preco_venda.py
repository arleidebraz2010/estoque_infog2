# Generated by Django 2.1.7 on 2019-03-15 19:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('produto', '0003_auto_20190315_1852'),
    ]

    operations = [
        migrations.AddField(
            model_name='produto',
            name='preco_venda',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
    ]
