from rest_framework.serializers import ModelSerializer
from produto_nota_saida.models import ProdutoNotaSaida
from estoque.models import Estoque
from movimento_produto.models import MovimentoProduto


class ProdutoNotaSaidaSerializer(ModelSerializer):
    class Meta:
        model = ProdutoNotaSaida
        fields = ('id',  'quantidade', 'valor_unitario', 'valor_desconto',
                  'produto', 'nota_saida')

    """
        Implementação do metodo create() para
        que possa gerar um estoque ao receber a mercadoria
    """

    def create(self, validated_data):
        produto_nota = ProdutoNotaSaida.objects.create(**validated_data)
        self.salvar_estoque(produto_nota)
        return produto_nota

    """
        Implementação do metodo update() para
        que possa alterar ou criar um estoque ao 
        alterar um item da nota
    """

    def update(self, instance, validated_data):
        produto_nota = ProdutoNotaSaida.objects.create(**validated_data)
        self.salvar_estoque(produto_nota)
        return produto_nota

    """
        Usaodo para gerar ou alterar o Estoque quando 
        houver o recebimento de mercadorias pelo create(), ou
        a alteração do item da mercadoria pelo update()
    """

    def salvar_estoque(self, produto_nota):
        estoque_existente = Estoque.objects.filter(produto=produto_nota.produto)
        estoque = Estoque()
        estoque_anterior = Estoque()

        for e in estoque_existente:
            estoque = e

        if not estoque.quantidade:
            novo_estoque = Estoque()
            novo_estoque.quantidade = 0 - produto_nota.quantidade
            novo_estoque.produto = produto_nota.produto

            # Passar estoque zerado para os produtos que nuncam tiveram estoque
            estoque_anterior.quantidade = 0
            self.gerar_movimento_produto(produto_nota, estoque_anterior)
            return novo_estoque.save()
        else:
            estoque.quantidade = estoque.quantidade - produto_nota.quantidade
            estoque.produto = produto_nota.produto
            self.gerar_movimento_produto(produto_nota, estoque)
            return estoque.save()

    """
        Gerar o movimento do produto ao receber a mercadoria,
        e alterar o estoque
    """
    def gerar_movimento_produto(self, produto_nota, estoque):
        movimento_produto = MovimentoProduto()
        movimento_produto.estoque_anterior = estoque.quantidade + produto_nota.quantidade
        movimento_produto.produto = produto_nota.produto
        movimento_produto.tipo_movimento = 'SAIDA'
        movimento_produto.numero_nota = produto_nota.nota_saida.numero_nota
        return movimento_produto.save()





