from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from produto_nota_saida.models import ProdutoNotaSaida
from .serializer import ProdutoNotaSaidaSerializer
from rest_framework.response import Response


class ProdutoNotaSaidaViewSet(ModelViewSet):
    serializer_class = ProdutoNotaSaidaSerializer

    def get_queryset(self):
        return ProdutoNotaSaida.objects.all()

    def list(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(ProdutoNotaSaidaViewSet, self).partial_update(request, *args, **kwargs)

    @action(methods=['post'], detail=False)
    def produtos_por_nota_saida(self, request):
        nota_id = request.data['nota_saida']
        produtos = ProdutoNotaSaida.objects.filter(nota_saida=nota_id)
        serializer = ProdutoNotaSaidaSerializer(produtos, many=True)
        return Response(serializer.data)

