from django.apps import AppConfig


class ProdutoNotaSaidaConfig(AppConfig):
    name = 'produto_nota_saida'
