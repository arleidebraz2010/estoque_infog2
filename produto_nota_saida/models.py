from django.db import models
from nota_saida.models import NotaSaida
from produto.models import Produto


class ProdutoNotaSaida(models.Model):
    quantidade = models.IntegerField(null=False)
    valor_unitario = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    valor_desconto = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, null=False, blank=True)
    nota_saida = models.ForeignKey(
        NotaSaida, on_delete=models.CASCADE, null=False, blank=True)

    def __str__(self):
        return self.produto.nome
