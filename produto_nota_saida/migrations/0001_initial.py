# Generated by Django 2.1.7 on 2019-03-16 05:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('produto', '0006_auto_20190315_1932'),
        ('nota_saida', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProdutoNotaSaida',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantidade', models.IntegerField()),
                ('valor_unitario', models.DecimalField(decimal_places=2, max_digits=10)),
                ('valor_desconto', models.DecimalField(decimal_places=2, max_digits=10)),
                ('nota_saida', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='nota_saida.NotaSaida')),
                ('produto', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='produto.Produto')),
            ],
        ),
    ]
