from django.db import models
from produto.models import Produto


class MovimentoProduto(models.Model):

    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, null=False, blank=True)
    tipo_movimento = models.CharField(null=False, max_length=10)
    numero_nota = models.IntegerField(null=True)
    estoque_anterior = models.IntegerField(null=True)
    data_movimento = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.produto.nome
