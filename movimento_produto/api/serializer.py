from rest_framework.serializers import ModelSerializer
from movimento_produto.models import MovimentoProduto


class MovimentoProdutoSerializer(ModelSerializer):
    class Meta:
        model = MovimentoProduto
        fields = ('id',  'produto', 'tipo_movimento', 'numero_nota',
                  'estoque_anterior', 'data_movimento')
