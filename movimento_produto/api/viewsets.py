from rest_framework.viewsets import ModelViewSet
from movimento_produto.models import MovimentoProduto
from .serializer import MovimentoProdutoSerializer

class MovimentoProdutoViewSet(ModelViewSet):
    serializer_class = MovimentoProdutoSerializer

    def get_queryset(self):
        return MovimentoProduto.objects.all()

    def list(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(MovimentoProdutoViewSet, self).partial_update(request, *args, **kwargs)


