from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from nota_saida.models import NotaSaida
from .serializer import NotaSaidaSerializer


class NotaSaidaViewSet(ModelViewSet):
    serializer_class = NotaSaidaSerializer

    def get_queryset(self):
        return NotaSaida.objects.all()

    def list(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(NotaSaidaViewSet, self).partial_update(request, *args, **kwargs)


