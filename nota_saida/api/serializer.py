from rest_framework.serializers import ModelSerializer
from nota_saida.models import NotaSaida


class NotaSaidaSerializer(ModelSerializer):

    class Meta:
        model = NotaSaida
        fields = ('id',  'numero_nota', 'serie', 'data_saida', 'valor_total',
                  'cfop', 'ativo')
