from django.apps import AppConfig


class NotaSaidaConfig(AppConfig):
    name = 'nota_saida'
