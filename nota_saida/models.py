from django.db import models


class NotaSaida(models.Model):
    numero_nota = models.IntegerField(null=False)
    serie = models.CharField(max_length=5, null=True)
    data_saida = models.DateField(null=True)
    valor_total = models.DecimalField(max_digits=10, decimal_places=2, null=False)
    cfop = models.CharField(max_length=4, null=True)
    ativo = models.BooleanField(default=True, null=False)

    def __str__(self):
        return str(self.numero_nota)
