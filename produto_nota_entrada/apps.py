from django.apps import AppConfig


class ProdutoNotaEntradaConfig(AppConfig):
    name = 'produto_nota_entrada'
