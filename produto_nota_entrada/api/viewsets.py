from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from produto_nota_entrada.models import ProdutoNotaEntrada
from .serializer import ProdutoNotaEntradaSerializer
from rest_framework.response import Response


class ProdutoNotaEntradaViewSet(ModelViewSet):
    serializer_class = ProdutoNotaEntradaSerializer

    def get_queryset(self):
        return ProdutoNotaEntrada.objects.all()

    def list(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(ProdutoNotaEntradaViewSet, self).partial_update(request, *args, **kwargs)

    @action(methods=['post'], detail=False)
    def produtos_por_nota_entrada(self, request):
        nota_id = request.data['nota_entrada']
        produtos = ProdutoNotaEntrada.objects.filter(nota_entrada=nota_id)
        serializer = ProdutoNotaEntradaSerializer(produtos, many=True)
        return Response(serializer.data)
