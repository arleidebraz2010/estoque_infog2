# Generated by Django 2.1.7 on 2019-03-15 20:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('produto_nota_entrada', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='produtonotaentrada',
            old_name='numero_nota',
            new_name='nota_entrada',
        ),
        migrations.RenameField(
            model_name='produtonotaentrada',
            old_name='desconto',
            new_name='valor_desconto',
        ),
    ]
