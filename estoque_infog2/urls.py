"""estoque_infog2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers
from produto.api.viewsets import ProdutoViewSet
from nota_entrada.api.viewsets import NotaEntradaViewSet
from produto_nota_entrada.api.viewsets import ProdutoNotaEntradaViewSet
from nota_saida.api.viewsets import NotaSaidaViewSet
from produto_nota_saida.api.viewsets import ProdutoNotaSaidaViewSet
from estoque.api.viewsets import EstoqueViewSet

router = routers.DefaultRouter()
router.register(r'produto', ProdutoViewSet, base_name='Produto')
router.register(r'nota_entrada', NotaEntradaViewSet, base_name='NotaEntrada')
router.register(r'produto_nota_entrada', ProdutoNotaEntradaViewSet, base_name='ProdutoNotaEntrada')
router.register(r'nota_saida', NotaSaidaViewSet, base_name='NotaSaida')
router.register(r'produto_nota_saida', ProdutoNotaSaidaViewSet, base_name='ProdutoNotaSaida')
router.register(r'estoque', EstoqueViewSet, base_name='Estoque')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
]
