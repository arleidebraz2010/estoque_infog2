from rest_framework.viewsets import ModelViewSet
from estoque.models import Estoque
from .serializer import EstoqueSerializer
from rest_framework.decorators import action
from rest_framework.response import Response


class EstoqueViewSet(ModelViewSet):
    serializer_class = EstoqueSerializer

    def get_queryset(self):
        return Estoque.objects.all()

    def list(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(EstoqueViewSet, self).partial_update(request, *args, **kwargs)

    """
        Endpoint extra para listar o estoque passando o 
        código do produto como parametro pelo metodo
        post, já que por padao o id que é acionado no GET,
        e achei melhor criar este endpoint, ao inves de 
        dar um override no retorno padrão
    """
    @action(methods=['post'], detail=False)
    def estoque_por_produtos(self, request):
        produto_id = request.data['produto']
        estoque = Estoque.objects.filter(produto=produto_id)
        serializer = EstoqueSerializer(estoque, many=True)
        return Response(serializer.data)

