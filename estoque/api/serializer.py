from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SerializerMethodField
from estoque.models import Estoque
from movimento_produto.models import MovimentoProduto
from movimento_produto.api.serializer import MovimentoProdutoSerializer


class EstoqueSerializer(ModelSerializer):
    movimento_produto = SerializerMethodField(read_only=True)

    class Meta:
        model = Estoque
        fields = ('id', 'quantidade', 'produto', 'movimento_produto')

    """
          Implementação do metodo create() e update() do estoque,
          não serão realizados  pois ao receber uma mercadoria,
          ou fazer uma saida, pelos endpoints de produto_nota_entrada 
          e produto_nota_saida, automaticamente faz a alteracao 
          do Estoque e gera uma movimentação. No caso ao criar pela primeira
          vez nao será gerado o movimento, sendo chamado o metodo apenas
          no update().
          
          Obs: Devido ao tempo nao foi implementado as tratativas
          como brrar os POST, PUT e DELETE do endpoint Estoque para evitar
          alteração no estoque de forma manual      
    """

    """
        get_movimento_produto irá retornar o movimento dos
        produtos listados.
    """
    def get_movimento_produto(self, obj):
        movimento = MovimentoProduto.objects.filter(produto=obj.produto)
        serializer = MovimentoProdutoSerializer(movimento, many=True)
        return serializer.data


