from django.db import models
from produto.models import Produto


class Estoque(models.Model):
    quantidade = models.IntegerField(null=True)
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE, null=False, blank=True)

    def __str__(self):
        return self.produto.nome
