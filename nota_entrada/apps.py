from django.apps import AppConfig


class NotaEntradaConfig(AppConfig):
    name = 'nota_entrada'
