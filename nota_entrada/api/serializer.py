from rest_framework.serializers import ModelSerializer
from nota_entrada.models import NotaEntrada


class NotaEntradaSerializer(ModelSerializer):
    class Meta:
        model = NotaEntrada
        fields = ('id',  'numero_nota', 'serie', 'data_emissao', 'data_eentrada', 'valor_total',
                  'cfop', 'ativo')
