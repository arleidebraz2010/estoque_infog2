from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from nota_entrada.models import NotaEntrada
from .serializer import NotaEntradaSerializer


class NotaEntradaViewSet(ModelViewSet):
    serializer_class = NotaEntradaSerializer

    def get_queryset(self):
        return NotaEntrada.objects.all()

    def list(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).retrieve(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return super(NotaEntradaViewSet, self).partial_update(request, *args, **kwargs)


